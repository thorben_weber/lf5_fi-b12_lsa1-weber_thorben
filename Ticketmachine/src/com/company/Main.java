package com.company;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner tastatur = new Scanner(System.in);

        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        double rueckgabebetrag;
        double anzahlFahrkarte;

        fahrkartenBestellungErfassen(zuZahlenderBetrag);
        fahrkartenBezahl(zuZahlenderBetrag, eingezahlterGesamtbetrag, eingeworfeneMuenze);
        rueckgeldAusgeben();

        System.out.println("Anzahl der Fahrkarten: ");
        anzahlFahrkarte = tastatur.nextInt();
        zuZahlenderBetrag = anzahlFahrkarte * zuZahlenderBetrag;

        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
            if (eingeworfeneMuenze > 2) {
                System.out.println("Bitte werfen sie nur Münzen im Wert von 2€ oder niedriger ein.");
                zuZahlenderBetrag = eingeworfeneMuenze + zuZahlenderBetrag;
            }
        }
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wünschen Ihnen eine gute Fahrt.");

    }

    public static double fahrkartenBestellungErfassen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);
        System.out.println();
        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        return zuZahlenderBetrag;
    }

    public static double fahrkartenBezahl(double eingezahlterGesamtbetrag, double zuZahlenderBetrag, double eingeworfeneMuenze) {
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
            if (eingeworfeneMuenze > 2) {
                System.out.println("Bitte werfen sie nur Münzen im Wert von 2€ oder niedriger ein.");
                zuZahlenderBetrag = eingeworfeneMuenze + zuZahlenderBetrag;
            }
            return zuZahlenderBetrag;
        }
    }

    public static double rueckgeldAusgeben(double rueckgeld) {
        if (rueckgeld > 0) {
            System.out.println("Ihr Rückgeld beläuft sich auf:" + rueckgeld + "€");

            System.out.println("Achtung, du kriegen Geld, nimm oder Tot durch zerquetschen:");
        }
        while (rueckgeld >= 2.00) // 2 EURO-Münzen
        {
            System.out.println("2 EURO");
            rueckgeld -= 2.00;
        }
        while (rueckgeld >= 1.00) // 1 EURO-Münzen
        {
            System.out.println("1 EURO");
            rueckgeld -= 1.00;
        }
        while (rueckgeld >= 0.50) // 50 CENT-Münzen
        {
            System.out.println("50 CENT");
            rueckgeld -= 0.50;
        }
        while (rueckgeld >= 0.20) // 20 CENT-Münzen
        {
            System.out.println("20 CENT");
            rueckgeld -= 0.20;
        }
        while (rueckgeld >= 0.10) // 10 CENT-Münzen
        {
            System.out.println("10 CENT");
            rueckgeld -= 0.10;
        }
        while (rueckgeld >= 0.05)// 5 CENT-Münzen
        {
            System.out.println("5 CENT");
            rueckgeld -= 0.05;
        }
        return rueckgeld;
    }
}
