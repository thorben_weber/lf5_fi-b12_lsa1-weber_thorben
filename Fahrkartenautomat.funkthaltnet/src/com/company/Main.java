package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner tastatur = new Scanner(System.in);

        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        double rueckgabebetrag;
        double anzahlFahrkarte;

        fahrkartenBestellungErfassen(zuZahlenderBetrag);
        fahrkartenBezahl(zuZahlenderBetrag, eingezahlterGesamtbetrag, eingeworfeneMuenze);
        rueckgeldAusgeben()

        System.out.println("Anzahl der Fahrkarten: ");
        anzahlFahrkarte = tastatur.nextInt();
        zuZahlenderBetrag = anzahlFahrkarte * zuZahlenderBetrag;

        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
            if (eingeworfeneMuenze > 2) {
                System.out.println("Bitte werfen sie nur Münzen im Wert von 2€ oder niedriger ein.");
                zuZahlenderBetrag = eingeworfeneMuenze + zuZahlenderBetrag;
            }
        }
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if (rueckgabebetrag > 0.00) {
            System.out.println("Der Rückgabebetrag in Höhe von " + rueckgabebetrag + " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");
            while (rueckgabebetrag >= 2.00) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.00;
            }
            while (rueckgabebetrag >= 1.00) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.00;
            }
            while (rueckgabebetrag >= 0.50) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.50;
            }
            while (rueckgabebetrag >= 0.20) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.20;
            }
            while (rueckgabebetrag >= 0.10) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.10;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wünschen Ihnen eine gute Fahrt.");

    }

    public static double fahrkartenBestellungErfassen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);
        System.out.println();
        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        return zuZahlenderBetrag;
    }

    public static double fahrkartenBezahl(double eingezahlterGesamtbetrag, double zuZahlenderBetrag, double eingeworfeneMuenze) {
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
            if (eingeworfeneMuenze > 2) {
                System.out.println("Bitte werfen sie nur Münzen im Wert von 2€ oder niedriger ein.");
                zuZahlenderBetrag = eingeworfeneMuenze + zuZahlenderBetrag;
            }
            return zuZahlenderBetrag;
        }
    }

    public static double rueckgeldAusgeben() {
        System.out.println("Ihr Rückgeld beläuft sich auf:" + rueckgeld);
        // if zuZahlenderBetrag < eingeworfeneMuenze
        // dann eingeworfeneMuenze - zuZahlenderBetrag = rueckgeld

        //


    }
}